﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Card object. A card had a value and methods to animate its movement and destruction.
/// </summary>
public class Card : MonoBehaviour {
	
	[SerializeField]
	private MeshRenderer frontMesh;
	[SerializeField]
	private ParticleSystem burnParticles;
	[SerializeField]
	private Color burntColor;
	
	private int _value;
	public int Value { get {return _value;}}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Sets the value of the card and loads the corresponding texture onto the front mesh.
	/// </summary>
	/// <param name="value">Value.</param>
	public void SetValue(int value)
	{
		_value = value;
		string textureName = "card" + value.ToString();
		if (value == StaticValues.knightCardValue)
		{
			textureName = StaticValues.knightCardName;
		}

		Texture2D frontTexture = Resources.Load<Texture2D>(StaticValues.cardFacePath + "/" + textureName);
		frontMesh.material.mainTexture = frontTexture;
	}


	/// <summary>
	/// Starts a coroutine that animates the card to a given position and rotation
	/// </summary>
	/// <param name="target">Target position.</param>
	/// <param name="targetRotation">Target rotation.</param>
	/// <param name="time">The duration of the animation.</param>
	public void AnimateToPosition(Vector3 target, Vector3 targetRotation, float time)
	{
		StartCoroutine(AnimateToPosition_R(target, targetRotation, time));
	}

	bool animating = false;
	/// <summary>
	/// Coroutine for the card animation.
	/// </summary>
	public IEnumerator AnimateToPosition_R(Vector3 target, Vector3 targetRotation, float time)
	{
		float timer = time;
		Vector3 startPosition = this.transform.position;
		Vector3 startRotation = this.transform.localRotation.eulerAngles;
		animating = true;

		while (animating && timer > 0)
		{
			timer -= Time.deltaTime;
			if (timer < 0)
				break;
			this.transform.position = startPosition + (target - startPosition) * (1f-(timer / time));
			this.transform.localRotation = Quaternion.Euler(startRotation + (targetRotation - startRotation) * (1f-(timer / time)));
			yield return null;
		}
		if (animating)
		{
			this.transform.position = target;
			this.transform.localRotation = Quaternion.Euler(targetRotation);
		}
		animating = false;
		yield return null;
	}

	/// <summary>
	/// Starts a burn animation for the card.
	/// </summary>
	/// <param name="time">Duration of the animation.</param>
	public void BurnCard(float time)
	{
		StartCoroutine(burnCard_R(time));
	}

	/// <summary>
	/// Coroutine for the burn animation.
	/// </summary>
	private IEnumerator burnCard_R(float time)
	{
		Color startColor = frontMesh.material.color;
		burnParticles.Play();
		float timer = time;
		while (timer > 0)
		{
			timer -= Time.deltaTime;
			frontMesh.material.color = startColor + (burntColor - startColor) * (1f-(timer / time));
			yield return null;
		}
		burnParticles.Stop();
		yield return null;
	}
}
