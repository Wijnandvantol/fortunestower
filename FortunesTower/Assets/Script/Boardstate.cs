﻿using UnityEngine;
using System.Collections;
using System;

public class Boardstate : Singleton<Boardstate> {

	[SerializeField]
	private Deck deck;
	[SerializeField]
	private GameObject cardPrefab;
	[SerializeField]
	private int maxRows = 8;
	[SerializeField]
	private int startMoney = 200;
	[SerializeField]
	private float dealTime = 0.4f;
	[SerializeField]
	private float cardDealWaitTime = 0.1f;
	[SerializeField]
	private float cardBurnTime = 0.2f;
	[SerializeField]
	private int betAmount = 15;
	
	private int currentRow;
	private Card [][] pyramid;
	private bool gameOver;
	private int money;

	void Awake()
	{
		money = startMoney;
	}

	// Use this for initialization
	void Start () {

	}

	/// <summary>
	/// Starts a coroutine for restarting the boardstate.
	/// </summary>
	public void Restart()
	{
		StartCoroutine(restart_R());
	}	


	/// <summary>
	/// Coroutine for restarting the boardstate
	/// </summary>
	private IEnumerator restart_R()
	{
		UIController.Instance.EnableButtons(false);
		money -= betAmount;
		UIController.Instance.SetTurnOut(0);
		UIController.Instance.SetCash(money);
		clearPyramid();
		deck.InitDeck();
		dealGateCard();
		yield return StartCoroutine(dealRow_R());
		yield return StartCoroutine(calculateWinnings_R());
		UIController.Instance.EnableButtons(true);
	}

	/// <summary>
	/// Removes all the cards in the card pyramid. If the pyramid is null, it creates a new, empty pyramid.
	/// </summary>
	private void clearPyramid()
	{
		if (pyramid == null)
		{
			pyramid = new Card[maxRows][];
			return;
		}
		for (int i = 0; i < pyramid.Length; i++)
		{
			if (pyramid[i] == null)
			{
				continue;
			}
			for (int j = 0; j < pyramid[i].Length; j++)
			{
				if (pyramid[i][j] != null)
				{
					Destroy(pyramid[i][j].gameObject);
				}
			}
		}
		pyramid = new Card[maxRows][];
	}

	// Update is called once per frame
	void Update () {
	
	}	

	/// <summary>
	/// Starts a coroutine that executes the game sequence.
	/// </summary>
	public void ExecuteGameSequence()
	{
		StartCoroutine(gameSequence_R());
	}

	/// <summary>
	/// Starts a coroutine for the payout sequence.
	/// </summary>
	public void Payout()
	{
		StartCoroutine(payout_R());
	}

	/// <summary>
	/// Deals the gate card. This card is dealt closed as the top card of the pyramid.
	/// </summary>
	private void dealGateCard()
	{
		int value = deck.DrawCard();
		currentRow = 0;
		pyramid[currentRow] = new Card[1];
		dealCard(value, currentRow, 0 , false);
	}

	/// <summary>
	/// Starts a coroutine that deals a row of cards.
	/// </summary>
	private void dealRow()
	{
		StartCoroutine(dealRow_R());
	}

	/// <summary>
	/// Starts a coroutine that deals a card.
	/// </summary>
	/// <param name="value">The value of the card to deal.</param>
	/// <param name="row">The row that the card should be dealt in.</param>
	/// <param name="cardNr">The index of the card in the row.</param>
	/// <param name="open">If set to <c>true</c> , the card is dealt open. Otherwise it is dealt closed.</param>
	private void dealCard(int value, int row, int cardNr, bool open = true)
	{
		StartCoroutine( dealCard_R( value, row, cardNr, open));
	}

	/// <summary>
	/// The coroutine for the game sequence. It first deals a row.
	/// Then checks the row that has been dealt for cards that should burn.
	/// Then, if the player is gameover, handle the game over sequence.
	/// Else, if it is the last row calculate the winnings and pay them out.
	/// Otherwise, just calculate the current winnings.
	/// </summary>
	private IEnumerator gameSequence_R()
	{
		UIController.Instance.EnableButtons(false);
		yield return StartCoroutine(dealRow_R());
		yield return StartCoroutine(checkCurrentRow_R());

		if (gameOver)
		{
			UIController.Instance.SetTurnOut(0);
			UIController.Instance.OnGameOver();
		}
		else if (currentRow == maxRows -1)
		{
			yield return StartCoroutine(calculateWinnings_R());
			UIController.Instance.OnGameOver();
			Payout();
		}
		else
		{
			yield return StartCoroutine(calculateWinnings_R());
			UIController.Instance.EnableButtons(true);
		}
	}

	/// <summary>
	/// Coroutine for dealing a row of cards. 
	/// It draws a value from the deck, and deals a card with that value to the given position in the pyramid.
	/// </summary>
	private IEnumerator dealRow_R( )
	{
		if (currentRow >= maxRows-1)
			yield break;
		currentRow++;
		pyramid[currentRow] = new Card[currentRow +1];
		for (int i = 0; i <= currentRow; i++)
		{
			int value = deck.DrawCard();
			dealCard(value,currentRow, i);
			yield return new WaitForSeconds(cardDealWaitTime);
		}
		yield return new WaitForSeconds(dealTime + cardDealWaitTime);
	}

	/// <summary>
	/// Coroutine for dealing a card. It instantiates a card from a prefab and sets the given value. 
	/// Then it animates the card to the given position.
	/// </summary>
	/// <param name="value">Value of the card to be dealt.</param>
	/// <param name="row">The target row.</param>
	/// <param name="cardNr">The target index of the card in the target row.</param>
	/// <param name="open">If set to <c>true</c>, the card is dealt open. Otherwise it is dealt closed.</param>
	private IEnumerator dealCard_R(int value, int row, int cardNr, bool open = true)
	{
		Vector3 position = getPosition(row, cardNr);
		GameObject cardGO = Instantiate(cardPrefab, deck.transform.position + new Vector3(0,0.05f,0), Quaternion.Euler(0,0,180)) as GameObject;
		cardGO.transform.SetParent(this.transform);
		Vector3 targetRotation;
		if (open)
		{
			targetRotation = new Vector3(0,0,0);
		}
		else 
		{
			targetRotation = new Vector3(0,0,180);
		}
		Card card = cardGO.GetComponent<Card>();
		pyramid[row][cardNr] = card;
		if (card != null)
		{
			card.SetValue(value);
			card.AnimateToPosition(position, targetRotation,  dealTime);
		}
		yield return null;
	}

	/// <summary>
	/// Coroutine for using the gate card. 
	/// If it is not used, it moves the top card of the pyramid to the given position.
	/// It then removes the card at the given position and substitutes it with the gate card.
	/// Finally it sets the top position of the pyramid to null, to indicate the gate card has been used (it is no longer at that position).
	/// </summary>
	/// <param name="row">Row of the card to replace.</param>
	/// <param name="cardNr">Index in the row of the card to replace.</param>
	private IEnumerator useGateCard_R(int row, int cardNr)
	{
		if (pyramid[0][0] == null)
		{
			yield break;
		}
		Vector3 position = getPosition(row, cardNr);
		yield return StartCoroutine(pyramid[0][0].AnimateToPosition_R(position + new Vector3(0,0.2f,0), new Vector3(0,0,0),  dealTime));
		Destroy(pyramid[row][cardNr].gameObject);
		pyramid[row][cardNr] = pyramid[0][0];
		pyramid[0][0] = null;
		yield return StartCoroutine(pyramid[row][cardNr].AnimateToPosition_R(position, new Vector3(0,0,0),  cardDealWaitTime));
		yield return new WaitForSeconds(cardDealWaitTime);
	}

	/// <summary>
	/// Coroutine for checking the current row.
	/// Checks each card of a row if it has a card with the same value above it. If it does, it burns the card.
	/// If the gatecard has not been used, it uses the gatecard and checks the card again.
	/// Otherwise, the player is game over.
	/// If the row contains a knight card or the row is a set, then the row is safe.
	/// </summary>
	private IEnumerator checkCurrentRow_R()
	{
		gameOver = false;
		if (Array.Exists(pyramid[currentRow], (c => c.Value == StaticValues.knightCardValue)) || rowIsSet(currentRow))
		{
			yield break;
		}

		for (int i = 0; i < pyramid[currentRow].Length; i++)
		{
			if (checkCard(currentRow, i))
			{
				pyramid[currentRow][i].BurnCard(cardBurnTime);
				if (pyramid[0][0] != null)
				{
					yield return new WaitForSeconds(cardBurnTime + cardDealWaitTime);
					yield return StartCoroutine(useGateCard_R(currentRow, i));
					//extra check for the knight card, in case the gate card is a knight card
					if (pyramid[currentRow][i].Value == StaticValues.knightCardValue)
						yield break;
					i--;
				}
				else 
				{
					gameOver = true;
				}
			}
		}
		yield return null;
	}

	/// <summary>
	/// Coroutine for calculating the winnings. 
	/// This is a coroutine to be consistent with the rest of the game sequence, and to allow for the possibility of an animation.
	/// </summary>
	private IEnumerator calculateWinnings_R()
	{
		UIController.Instance.SetTurnOut(calculateWinnings());
		yield return null;
	}

	/// <summary>
	/// Coroutine for paying out the winnings.
	/// The winnings get added to the players gold and this is shown in the UI.
	/// </summary>
	private IEnumerator payout_R()
	{
		UIController.Instance.EnableRestart(false);
		int winnings = calculateWinnings();
		while (winnings > 0)
		{
			winnings--;
			money++;
			UIController.Instance.SetCash(money);
			UIController.Instance.SetTurnOut(winnings);
			yield return null;
		}
		UIController.Instance.EnableRestart(true);
	}

	/// <summary>
	/// Checks a card if there is a card with the same value above it.
	/// </summary>
	/// <returns><c>true</c>, if card has a card of the same value above it, <c>false</c> otherwise.</returns>
	/// <param name="row">Row of the card to check.</param>
	/// <param name="card">Index of the card to check.</param>
	private bool checkCard(int row, int card)
	{
		bool leftSame = false;
		bool rightSame = false;
		if (card > 0)
		{
			leftSame = (pyramid[row][card].Value == pyramid[row-1][card-1].Value);
		}
		if (card < row)
		{
			rightSame = (pyramid[row][card].Value == pyramid[row-1][card].Value);
		}
		return rightSame || leftSame;
	}

	/// <summary>
	/// Checks whether all cards in the row are the same, i.e. a set.
	/// </summary>
	/// <returns><c>true</c>, if the row is a set <c>false</c> otherwise.</returns>
	/// <param name="row">The row to check</param>
	private bool rowIsSet(int row)
	{
		for (int i = 0; i < row; i++)
		{
			if (pyramid[row][i].Value != pyramid[row][i+1].Value)
			{
				return false;
			}
		}
		return true;
	}

	/// <summary>
	/// Calculates the current winnings. 
	/// This can be just the value of the current row or, in case of the jackpot, the addition of all the rows.
	/// </summary>
	/// <returns>The winnings.</returns>
	private int calculateWinnings()
	{
		// if it is the last row and the gate card isn't used you hit the jackpot
		if (currentRow == maxRows -1 && pyramid[0][0] != null)
		{
			int winnings = 0;
			for (int i = 1; i < maxRows; i++)
			{
				winnings += calculateRowValue(i);
			}
			return winnings;
		}
		else 
		{
			return calculateRowValue(currentRow);
		}
	}

	/// <summary>
	/// Calculates the row value.
	/// This is the added value of all the cards in the row.
	/// This is multiplied by the number of cards in the row if the row is a set.
	/// </summary>
	/// <returns>The row value.</returns>
	/// <param name="row">The row to calculate.</param>
	private int calculateRowValue(int row)
	{
		int total = 0;
		foreach (Card c in pyramid[row])
		{
			total += Mathf.Max(0,c.Value);
		}
		if (rowIsSet(row))
		{
			total *= row+1;
		}
		return total;
	}

	/// <summary>
	/// Gets the world position of a card in a row.
	/// </summary>
	/// <returns>The position.</returns>
	/// <param name="row">Row.</param>
	/// <param name="card">Card.</param>
	private Vector3 getPosition(int row, int card)
	{
		float ySpacing = 0.20f;
		float xSpacing = 0.25f; 
		float y = -ySpacing * ((float)row  - (float)(maxRows -1) * 0.5f);
		float x = xSpacing * ((float)card  - (float)(row) * 0.5f);
		return new Vector3(x, (float)row * 0.01f, y);
	}
}
