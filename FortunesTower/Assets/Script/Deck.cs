﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Deck object. Contains a list of values which is the decklist.
/// Contains methods to initialize the deck and draw cards from it.
/// </summary>
public class Deck : MonoBehaviour {

	[SerializeField]
	private int numberedCardAmount = 10;
	[SerializeField]
	private int knightAmount = 4;

	private List<int> values;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Initializes the deck. Creates a list of "numberedCardAmount" times the numbers 1 - 7 and "knightAmount" times a knight card.
	/// </summary>
	public void InitDeck()
	{
		values = new List<int>();
		//for each numbered card (1..7)
		for (int j = 1; j <= 7; j++)
		{
			//add that number as much as the "numbered card amount"
			for (int i = 0; i < numberedCardAmount; i++)
			{
					values.Add(j);
			}
		}
		for (int k = 0; k < knightAmount; k++)
		{
			values.Add(StaticValues.knightCardValue);
		}
		values.Shuffle();
	}

	/// <summary>
	/// Draws a card and return the value. REMOVES THE VALUE FROM THE TOP OF THE DECK!
	/// </summary>
	/// <returns>The card.</returns>
	public int DrawCard()
	{
		int result = values[0];
		values.RemoveAt(0);
		return result;
	}
}
