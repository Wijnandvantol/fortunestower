﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Start object for initializing the scene. Now only used for setting the sort mode of the camera.
/// </summary>
public class StartObject : MonoBehaviour {

	void Awake()
	{
		Camera.main.transparencySortMode = TransparencySortMode.Orthographic;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
