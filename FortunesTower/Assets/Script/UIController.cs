﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// User interface controller for interacting wit hthe UI elements.
/// </summary>
public class UIController : Singleton<UIController> {

	[SerializeField]
	private Button dealRowButton;
	[SerializeField]
	private Button restartButton;
	[SerializeField]
	private Button foldButton;
	[SerializeField]
	private Text moneyText;
	[SerializeField]
	private Text turnoutText;

	void Start()
	{
		dealRowButton.gameObject.SetActive(true);
		restartButton.gameObject.SetActive(false);
		Boardstate.Instance.Restart();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
	}

	/// <summary>
	/// Enables or disables the buttons.
	/// </summary>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	public void EnableButtons(bool enable)
	{
		dealRowButton.interactable = enable;
		foldButton.interactable = enable;
		restartButton.interactable = enable;
	}

	/// <summary>
	/// Enables or disables the fold button
	/// </summary>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	public void EnableFold(bool enable)
	{
		foldButton.interactable = enable;
	}

	/// <summary>
	/// Enables or disables the deal button
	/// </summary>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	public void EnableDeal(bool enable)
	{
		dealRowButton.interactable = enable;
	}

	/// <summary>
	/// Enables or disables the restart button
	/// </summary>
	/// <param name="enable">If set to <c>true</c> enable.</param>
	public void EnableRestart(bool enable)
	{
		restartButton.interactable = enable;
	}


	public void OnGameOver()
	{
		dealRowButton.gameObject.SetActive(false);
		restartButton.gameObject.SetActive(true);
		restartButton.interactable = true;
		foldButton.interactable = false;
	}

	/// <summary>
	/// Deals a new row of cards. Called by the "Deal Row" button.
	/// </summary>
	public void DealRow()
	{
		Boardstate.Instance.ExecuteGameSequence();
	}

	/// <summary>
	/// Restart the game. Called by the "Restart" button.
	/// </summary>
	public void Restart()
	{
		dealRowButton.gameObject.SetActive(true);
		restartButton.gameObject.SetActive(false);
		EnableButtons(true);
		Boardstate.Instance.Restart();
	}

	/// <summary>
	/// Cash out the current row. Called by the "Fold" button.
	/// </summary>
	public void Fold()
	{
		OnGameOver();
		Boardstate.Instance.Payout();
	}

	/// <summary>
	/// Sets the turnout text, which displays the current turn out.
	/// </summary>
	/// <param name="money">Money.</param>
	public void SetTurnOut(int money)
	{
		if (money == 0)
		{
			turnoutText.text = "-";
		}
		else
		{
			turnoutText.text = money.ToString();
		}
	}

	/// <summary>
	/// Sets the cash text, which displays the players total amount of money.
	/// </summary>
	/// <param name="money">Money.</param>
	public void SetCash(int money)
	{
		moneyText.text = money.ToString();
	}

}
